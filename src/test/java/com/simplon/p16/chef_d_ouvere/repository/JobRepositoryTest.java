package com.simplon.p16.chef_d_ouvere.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.simplon.p16.chef_d_ouvere.entity.Job;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

@ActiveProfiles("test")
@SpringBootTest
@Sql(scripts={"/schema.sql", "/data.sql"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
public class JobRepositoryTest {

    @Autowired
    JobRepository jobRepo;

    @Test
    void testDeleteById() {
        assertTrue(jobRepo.deleteById(1));
    }

    @Test
    void testFindAll() {
        assertNotEquals(0, jobRepo.findAll().size());
    }

    @Test
    void testFindById() {
       Job job = jobRepo.findById(1);
        assertEquals(1, job.getJob_id());
       
    }

    @Test
    void testSave() {
        Job job = new Job("Melissa", "belle gentil");
        assertTrue(jobRepo.save(job));
        assertEquals(2, job.getJob_id());
        
    }

    @Test
    void testUpdate() {
        Job job = new Job(1, "Nico","super hero");
        assertTrue(jobRepo.update(job));
    }

    }

