package com.simplon.p16.chef_d_ouvere.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.simplon.p16.chef_d_ouvere.DAO.UserDAO;
import com.simplon.p16.chef_d_ouvere.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository implements UserDetailsService, UserDAO {
    @Autowired
    @Lazy
    private DataSource dataSource;

    private Connection connection;

    private String user_name;

    @Override
    public List<User> findAll() {

        List<User> list = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM user");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new User(
                                result.getInt("user_id"),
                                result.getString("user_name"),
                                result.getString("user_password"),
                                result.getString("user_role"),
                                result.getString("user_mobile"),
                                result.getString("user_city"),
                                result.getString("user_address")));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    @Override
    public User findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM user WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("user_id"),
                        result.getString("user_name"),
                        result.getString("user_password"),
                        result.getString("user_role"),
                        result.getString("user_mobile"),
                        result.getString("user_city"),
                        result.getString("user_address"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return null;
    }

    @Override

    public boolean save(User user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "INSERT INTO user (user_name,user_password,user_role, user_mobile, user_city, user_address) VALUES (?,?,?,?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getUser_name());
            stmt.setString(2, user.getUser_password());
            stmt.setString(3, user.getUser_role());
            stmt.setString(4, user.getUser_mobile());
            stmt.setString(5, user.getUser_address());
            stmt.setString(6, user.getUser_city());

            // cette partie sert à récupérer la primary key auto incrémenté et à l'assigner
            // à l'instance de User qu'on vient de faire persister
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setUser_id(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;

    }

    @Override
    public boolean update(User user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE user SET user_name=?,user_password=?,user_role=?, WHERE id=?");

            stmt.setString(1, user.getUser_name());
            stmt.setString(2, user.getUser_password());
            stmt.setString(3, user.getUser_role());
            stmt.setString(4, user.getUser_mobile());
            stmt.setString(5, user.getUser_address());
            stmt.setString(6, user.getUser_city());
            stmt.setInt(7, user.getUser_id());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("Delete From job where job_id =?");

            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;

    }

    @Override
    /**
     * To find a user by userName.
     * 
     * @param user name
     * @return
     */
    public User findByUserName(String userName) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("SELECT * FROM user WHERE user_name=?");
            stmt.setString(1, userName);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("user_id"),
                        result.getString("user_name"),
                        result.getString("user_password"),
                        result.getString("user_role"),
                        result.getString("user_mobile"),
                        result.getString("user_city"),
                        result.getString("user_address"));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * La méthode de l'interface pour Spring, au final dedans on ne fait qu'appeler
     * notre
     * méthode findByEmail (vu que dans notre cas notre email est notre username)
     * 
     * @param username Le username (ici l'email) du user recherché
     * @return Le User correspondant à l'email
     * @throws UsernameNotFoundException On fait en sorte de déclencher une
     *                                   exception si aucun User ne correspond à
     *                                   l'email donné (c'est spring qui veut ça)
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        this.user_name = username;
        User user = findByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

}
