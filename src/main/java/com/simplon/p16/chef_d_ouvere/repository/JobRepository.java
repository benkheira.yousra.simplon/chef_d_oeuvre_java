package com.simplon.p16.chef_d_ouvere.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.simplon.p16.chef_d_ouvere.DAO.JobDAO;
import com.simplon.p16.chef_d_ouvere.entity.Job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

@Repository
public class JobRepository implements JobDAO {
    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Job> findAll() {
        List<Job> list = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM job");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Job(
                                result.getInt("job_id"),
                                result.getString("job_name"),
                                result.getString("job_description")

                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Méthode qui ajoute un job en base de données
     * 
     * @param job Le job à ajouter en base de données
     * @return true si ça à marcher, false sinon
     */
    @Override
    public boolean save(Job job) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO job (job_name,job_description) VALUES (?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, job.getJob_name());
            stmt.setString(2, job.getJob_description());

            // cette partie sert à récupérer la primary key auto incrémenté et à l'assigner
            // à l'instance de User qu'on vient de faire persister
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                job.setJob_id(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    @Override
    public boolean update(Job job) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE job SET job_name=?,job_description=? WHERE job_id=?");

            stmt.setString(1, job.getJob_name());
            stmt.setString(2, job.getJob_description());
            stmt.setInt(3, job.getJob_id());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    /**
     * Méthode permettant de récupérer un job en se basant sur l'id
     * 
     * @param id l'id du job recherché
     * @return Le job correspondant à l'id ou null si pas de job
     */

    @Override
    public List<Job> findByName(String job) {
        List<Job> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM job WHERE job_name=?");
            stmt.setString(1, job);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Job(
                                result.getInt("job_id"),
                                result.getString("job_name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    @Override
    public Job findById(Integer id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM job WHERE job_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Job job = new Job(
                        result.getInt("job_id"),
                        result.getString("job_name"),
                        result.getString("job_description"));

                return job;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("Delete From job where job_id =?");

            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;

    }

}
