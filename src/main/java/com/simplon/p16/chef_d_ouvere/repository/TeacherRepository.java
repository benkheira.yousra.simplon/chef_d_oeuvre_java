package com.simplon.p16.chef_d_ouvere.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.simplon.p16.chef_d_ouvere.DAO.TeacherDAO;
import com.simplon.p16.chef_d_ouvere.entity.Teacher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

@Repository
public class TeacherRepository implements TeacherDAO {
    @Autowired
    private DataSource dataSource;
    private Connection connection;

    @Override
    public boolean deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("Delete From teacher where teacher_id =?");

            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;

    }

    @Override
    public List<Teacher> findAll() {
        List<Teacher> list = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM teacher");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Teacher(
                                result.getInt("teacher_id"),
                                result.getString("teacher_firstName"),
                                result.getString("teacher_lastName"),
                                result.getString("teacher_mobile"),
                                result.getString("teacher_email"),
                                result.getString("teacher_job")

                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    @Override
    public Teacher findById(Integer id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM teacher WHERE teacher_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Teacher teacher = new Teacher(
                        result.getInt("teacher_id"),
                        result.getString("teacher_firstName"),
                        result.getString("teacher_lastName"),
                        result.getString("teacher_mobile"),
                        result.getString("teacher_email"),
                        result.getString("teacher_job"));

                return teacher;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    @Override
    public List<Teacher> findByJob(String teacher) {
        List<Teacher> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM teacher WHERE teacher_job=?");
            stmt.setString(1, teacher);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Teacher(
                                result.getInt("teacher_id"),
                                result.getString("teacher_firstName"),
                                result.getString("teacher_lastName"),
                                result.getString("teacher_job")

                        ));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;

    }

    @Override
    public boolean save(Teacher teacher) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "INSERT INTO teacher (teacher_firstName,teacher_lastName,teacher_mobile, teacher_email, teacher_job) VALUES (?,?,?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, teacher.getTeacher_firstName());
            stmt.setString(2, teacher.getTeacher_lastName());
            stmt.setString(2, teacher.getTeacher_mobile());
            stmt.setString(2, teacher.getTeacher_email());
            stmt.setString(2, teacher.getTeacher_job());

            // cette partie sert à récupérer la primary key auto incrémenté et à l'assigner
            // à l'instance de User qu'on vient de faire persister
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                teacher.setTeacher_id(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;

    }

    @Override
    public boolean update(Teacher teacher) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "UPDATE techer SET teacher_firstName=?,teacher_lastName=?,teacher_mobile=?, teacher_email=?, teacher_job=? WHERE job_id=?");

            stmt.setString(1, teacher.getTeacher_firstName());
            stmt.setString(2, teacher.getTeacher_lastName());
            stmt.setString(2, teacher.getTeacher_mobile());
            stmt.setString(2, teacher.getTeacher_email());
            stmt.setString(2, teacher.getTeacher_job());
            stmt.setInt(3, teacher.getTeacher_id());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;

    }

}
