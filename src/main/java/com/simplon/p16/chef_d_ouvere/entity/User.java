package com.simplon.p16.chef_d_ouvere.entity;

import java.util.Collection;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class User implements UserDetails {
    private int user_id;
   
    @NotBlank
    private String user_name;
    private String user_mobile;
    @NotBlank
    @Size(min = 4,max = 45)
    private String user_password;
    private String user_address;
    private String user_city;
    private String user_role;

    public User(int user_id, @NotBlank String user_name) {
        this.user_id = user_id;
        this.user_name = user_name;
    }

    public User() {
    }

    public User(String user_name, String user_mobile, String user_password, String user_address, String user_city,
            String user_role) {
        this.user_name = user_name;
        this.user_mobile = user_mobile;
        this.user_password = user_password;
        this.user_address = user_address;
        this.user_city = user_city;
        this.user_role = user_role;
    }

    public User(int user_id, String user_name, String user_mobile, String user_password, String user_address,
            String user_city, String user_role) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_mobile = user_mobile;
        this.user_password = user_password;
        this.user_address = user_address;
        this.user_city = user_city;
        this.user_role = user_role;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_mobile() {
        return user_mobile;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public String getUser_city() {
        return user_city;
    }

    public void setUser_city(String user_city) {
        this.user_city = user_city;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(user_role));
    
    }

    @Override
    public String getPassword() {
        return user_password;
    }

    @Override
    public String getUsername() {
        return user_name;
    }

    @Override
    public boolean isAccountNonExpired() {
        
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //credential: methode de conx 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    

}
