package com.simplon.p16.chef_d_ouvere.entity;

public class Job {
    private int job_id;
    private String job_name;
    private String job_description;

    public Job(int job_id, String job_name) {
        this.job_id = job_id;
        this.job_name = job_name;
    }

    // constructors
    public Job() {
    }

    public Job(String job_name, String job_description) {
        this.job_name = job_name;
        this.job_description = job_description;
    }

    public Job(int job_id, String job_name, String job_description) {
        this.job_id = job_id;
        this.job_name = job_name;
        this.job_description = job_description;
    }

    // Getters & setter
    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getJob_description() {
        return job_description;
    }

    public void setJob_description(String job_description) {
        this.job_description = job_description;
    }

    @Override
    public String toString() {
        return "Job [job_description=" + job_description + ", job_name=" + job_name + "]";
    }

}
