package com.simplon.p16.chef_d_ouvere.entity;

public class Teacher {
    private int teacher_id;
    private String teacher_firstName;
    private String teacher_lastName;
    private String teacher_mobile;
    private String teacher_email;
    private String teacher_job;
    public Teacher(int teacher_id, String teacher_firstName, String teacher_lastName, String teacher_job) {
        this.teacher_id = teacher_id;
        this.teacher_firstName = teacher_firstName;
        this.teacher_lastName = teacher_lastName;
        this.teacher_job = teacher_job;
    }
    public Teacher() {
    }
    public Teacher(String teacher_firstName, String teacher_lastName, String teacher_mobile, String teacher_email,
            String teacher_job) {
        this.teacher_firstName = teacher_firstName;
        this.teacher_lastName = teacher_lastName;
        this.teacher_mobile = teacher_mobile;
        this.teacher_email = teacher_email;
        this.teacher_job = teacher_job;
    }
    public Teacher(int teacher_id, String teacher_firstName, String teacher_lastName, String teacher_mobile,
            String teacher_email, String teacher_job) {
        this.teacher_id = teacher_id;
        this.teacher_firstName = teacher_firstName;
        this.teacher_lastName = teacher_lastName;
        this.teacher_mobile = teacher_mobile;
        this.teacher_email = teacher_email;
        this.teacher_job = teacher_job;
    }
    public int getTeacher_id() {
        return teacher_id;
    }
    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }
    public String getTeacher_firstName() {
        return teacher_firstName;
    }
    public void setTeacher_firstName(String teacher_firstName) {
        this.teacher_firstName = teacher_firstName;
    }
    public String getTeacher_lastName() {
        return teacher_lastName;
    }
    public void setTeacher_lastName(String teacher_lastName) {
        this.teacher_lastName = teacher_lastName;
    }
    public String getTeacher_mobile() {
        return teacher_mobile;
    }
    public void setTeacher_mobile(String teacher_mobile) {
        this.teacher_mobile = teacher_mobile;
    }
    public String getTeacher_email() {
        return teacher_email;
    }
    public void setTeacher_email(String teacher_email) {
        this.teacher_email = teacher_email;
    }
    public String getTeacher_job() {
        return teacher_job;
    }
    public void setTeacher_job(String teacher_job) {
        this.teacher_job = teacher_job;
    }
    @Override
    public String toString() {
        return "Teacher [teacher_email=" + teacher_email + ", teacher_firstName=" + teacher_firstName + ", teacher_job="
                + teacher_job + ", teacher_lastName=" + teacher_lastName + ", teacher_mobile=" + teacher_mobile + "]";
    }

}
