package com.simplon.p16.chef_d_ouvere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
//(exclude = { SecurityAutoConfiguration.class }): pour enlever la page de spring boot par defaut
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ChefDOuvereApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChefDOuvereApplication.class, args);
	}

}
