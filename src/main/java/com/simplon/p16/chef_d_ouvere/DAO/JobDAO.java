package com.simplon.p16.chef_d_ouvere.DAO;

import java.util.List;

import com.simplon.p16.chef_d_ouvere.entity.Job;

/**
 * Classe représentant un Repository/DAO assez basique avec les méthodes du
 * CRUD les plus utilisées
 */
public interface JobDAO {
    /**
     * Récupère tous les jobs en base de données et les convertis en entity Job
     * @return Une liste d'instance de Job représentant les jobs contenus en BDD
     */
    List<Job> findAll();
    /**
     * Récupère un job spécifique en se basant sur son id
     * @param id L'id du Job à récupérer
     * @return Une instance de Job ou null si aucun job n'a cet id
     */
    Job findById(Integer id);
    /**
     * Méthode permettant de faire persister un job en base de données.
     * Son but sera de prendre une instance de job et de la convertir en une
     * requête SQL (INSERT INTO)
     * @param job L'instance de Job à faire persister en base de données
     */
    boolean save(Job job);
    /**
     * Méthode qui permet de mettre à jour un job déjà en base de données
     * (cette méthode n'existera pas vraiment dans les repository Spring
     * car le update est fait aussi à partir du save, mais on la met pour plus
     * de simplicité au début)
     * @param job Le job qu'on souhaite mettre à jour en base de données
     */
    boolean update(Job job);
    /**
     * Méthode qui permet de supprimer un job persisté en base de données en se
     * basant sur son id
     * @param id l'id du job à supprimer
     */
    boolean deleteById(Integer id);
    /**
     * 
     */
    List<Job> findByName(String name);
}

    

