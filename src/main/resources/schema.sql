DROP Table IF EXISTS job;
CREATE Table job (job_id INTEGER PRIMARY KEY AUTO_INCREMENT, job_name VARCHAR(50), job_description TEXT);
